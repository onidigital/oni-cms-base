<?php

namespace App\Observers;

use App\Models\User;
use Notification;

class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(User $user)
    {
        // Se foi cadastro pelo facebook, enviamos uma notificação:
        if( !empty($user->facebook_id) ){
            $notificaveis = $this->getNotificaveis();
            // enviamos uma notificação pro pessoal:
            Notification::send($notificaveis, new \App\Notifications\CadastroPeloFacebook($user));
        }
    }

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function updated(User $user)
    {
        
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  User  $user
     * @return void
     */
    public function deleting(User $user)
    {
        // Excluimos notificações referentes a este user:
        
    }

    /**
     * Quem recebe notificações?
     * retorna a lista de usuários que receberão uma notificação
     */
    public function getNotificaveis()
    {
        $users = User::whereHas('roles', function($query) {
            return $query->where('role_id', 1);
        })->get();
        return $users;
    }
}