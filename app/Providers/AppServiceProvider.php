<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;
use \Spatie\Permission\Models\Permission;
use App\Models\User;
use App\Observers\UserObserver;
use Auth;
use \Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Para corrigir o erro que deu ao instalar o Laravel 5.4:
         * "Syntax error or access violation: 1071 Specified key was too long;" 
         * Referencia: https://laravel-news.com/laravel-5-4-key-too-long-error
         */
        Schema::defaultStringLength(191);

        // Pra nossas datas ficarem no nosso idioma bem bunitim, global:
        Carbon::setLocale(config('app.locale'));

        // Validações customizadas:
        \Validator::extend('nome_proprio', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^[\pL\s]+$/u', $value);;
        });

        // Add todas as roles do config:
        $permissoes = [];
        if(Schema::hasTable('permissions')){
            // Add as permissoes do array pro BD:
            foreach(config('permissoes.itens') as $nome => $acoes){
                foreach($acoes as $key => $acao){
                    try{
                        Permission::updateOrCreate(['name' => $acao]);
                    }catch(\Exception $e){

                    }
                    $permissoes[] = $acao;
                }
            }
            // Removemos do BD permissoes que não estão mais no array:
            Permission::whereNotIn('name', $permissoes)->delete();

            // Dá um sync no admin caso tenhamos novas roles:
            $role_admin = Role::where('name', 'admin')->first();
            if($role_admin){
                $role_admin->permissions()->detach();
                try{
                    $role_admin->givePermissionTo(Permission::all());
                }catch(\Exception $e){

                }
            }
        }

        // Compartilhando o menu do sistema com determinada(s) view(s):
        view()->composer('admin/_partes/sidebar', function($view){
            $view->with('oni_menu', \App\Models\Menu::gerar_menu());
        });

        // Observer para "ouvir" eventos do model User
        User::observe(UserObserver::class);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
