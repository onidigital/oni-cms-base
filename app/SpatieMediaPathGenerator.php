<?php 

namespace App;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\PathGenerator\PathGenerator;

/**
 * Esta classe é para sobrescrever o caminho das imagens que são salvas
 * utilizando a biblioteca spatie/medialibrary
 * Até então elas são salvas na pasta com o nome sendo o id do registro(tabela media)
 * e as conversões ficam dentro da pasta com o nome da conversao + o formado(thumb.jpg, grande.jpg...)
 * E queremos que o nome da imagem fique com o nome original
 */

class SpatieMediaPathGenerator implements PathGenerator
{
    /*
     * Get the path for the given media, relative to the root storage path.
     */
    public function getPath(Media $media) : string
    {
    	//dd($media);
        //return md5($media->id).'/';
    }
    /*
     * Get the path for conversions of the given media, relative to the root storage path.
     * @return string
     */
    public function getPathForConversions(Media $media) : string
    {
        //return $this->getPath($media).'c/';
    }
}