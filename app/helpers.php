<?php

/**
 * Arquivo de funções auxiliares
 */

/**
 * Tooltip: gera um html com uma mensagem de ajuda e um ícone:
 */
function tooltip($title, $icone = '') {

	if(empty($icone)){
		$icone = 'fa fa-question';
	}

	return '<a href="javascript:void(0)" title="'.$title.'" class="help-tooltip">
				<i class="'.$icone.'"></i>
			</a>';
}