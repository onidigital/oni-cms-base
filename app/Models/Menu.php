<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;

/**
 * Model para gerenciar o menu do sistema
 * O menu vem do arquivo config/menu.php
 * Qualquer dúvida abra o arquivo citado antes de continuar
 */

class Menu extends Model
{
	/**
	 * Este método faz uso do pacote de menu para montá-lo
	 * retorna o html gerado
	 */
	public static function gerar_menu()
	{
		$menu_config = config('menu.itens');

		$menu_config = self::check_menu_ativo($menu_config);

		// para controlar os ids dos menus:
		$contador = 0;
		$menu_id = 'onimenu';

		// aqui guarda o html do menu renderizado
		$resultado = '<ul class="nav">'.PHP_EOL;

		// Percorremos o array e vamos montando o menu:
		if(is_array($menu_config) && (!empty($menu_config))){
			foreach($menu_config as $menu_nome => $dados){

				// se o usuário não tem permissão de ver este menu:
				$ir_para_proximo_menu = false;
				if(isset($dados['permissoes'])){
					// neste foreach provavelmente só vai ter a permissão listar tal coisa..
					foreach($dados['permissoes'] as $p){
						if(Auth::check() && !Auth::user()->can($p)){
							$ir_para_proximo_menu = true;
						}
					}
				}
				if($ir_para_proximo_menu)
					continue;

				// Se é o menu ativo:
				$active_class = '';
				if(isset($dados['active']) && ($dados['active']))
					$active_class = 'active';

				$resultado .= '<li class="'.$active_class.'">'.PHP_EOL;

				// gerando o id do menu para utilizar no collapse:
				$contador ++;
				$menu_id = $menu_id.$contador;

				// Verifica se tem url:
				if(!isset($dados['url']) || (isset($dados['url']) && (empty($dados['url']))) )
					$dados['url'] = '#';

				// verificamos se o menu tem filhos:
				if(self::tem_submenu($dados)){

					// Coloca um href diferente:
					$resultado .= '<a href="#'.$menu_id.'" data-toggle="collapse" >'.'<i class="ti'.$dados['icone'].'">'.'</i>'. $menu_nome .'</a>'.PHP_EOL;

					$collapse = 'collapse';
					// se é para dar "collapse in"
					if(isset($dados['collapse']) && ($dados['collapse']))
						$collapse .= ' in ';
					// inicia a div que abriga os submenus referentes a este $menu_id
					$resultado .= '<div class="'.$collapse.'" id="'.$menu_id.'">'.PHP_EOL;
					
					// iniciamos a ul nav necessária:
					$resultado .= '<ul class="nav">'.PHP_EOL;
					
					// percorremos cada filho:
					foreach($dados['submenus'] as $submenu_nome => $submenu_dados){

						// Permissões pelo submenu:
						// se o usuário não tem permissão de ver este menu:
						$ir_para_proximo_menu = false;
						if(isset($submenu_dados['permissoes'])){
							// neste foreach provavelmente só vai ter a permissão listar tal coisa..
							foreach($submenu_dados['permissoes'] as $p){
								if(!Auth::user()->can($p)){
									$ir_para_proximo_menu = true;
								}
							}
						}
						if($ir_para_proximo_menu)
							continue;
						
						// verifica se tem url pra não dar erro:
						if(!isset($submenu_dados['url']) || (isset($submenu_dados['url']) && (empty($submenu_dados['url']))) )
							$submenu_dados['url'] = '#sem-url';
						// Se é o submenu ativo:
						$active_class = '';
						if(isset($submenu_dados['active']) && ($submenu_dados['active']))
							$active_class = 'active';
						// faz o <li>
						$resultado .= '<li class="'.$active_class.'"><a href="'.url($submenu_dados['url']).'">'.$submenu_nome.'</a></li>'.PHP_EOL;
					}
					// fecha a ul nav:
					$resultado .= '</ul>'.PHP_EOL;
					// fecha a div que abriga os submenus:
					$resultado .= '</div>'.PHP_EOL;

				}else{

					// se o menu não tem filhos:
					$resultado .= '<a id="'.$menu_id.'" href="'.url($dados['url']).'" class="'.$active_class.'" >'.PHP_EOL;
					$resultado .= '<i class="'.$dados['icone'].'">'.'</i>'.PHP_EOL;
					$resultado .= '<p>'. $menu_nome .'</p></a>'.PHP_EOL;
				}

				$resultado .= '</li>'.PHP_EOL;
			}
		}else{
			// Nada no arquivo config/menu.php:
			return 'Arquivo config/menu vazio.';
		}

		// fim do menu
		$resultado .= '</ul>'.PHP_EOL;
		return $resultado;
	}

	// checa se um array de menu tem submenus:
	public static function tem_submenu($array)
	{
		if(isset($array['submenus']) && (!empty($array['submenus'])))
			return true;
		return false;
	}

	/**
	 * Este método verifica se a url atual corresponde a um menu clicado
	 * com o objetivo de deixá-lo em destaque/collapse após a página recarregar.
	 * retorna o array com a opção de menu destaque ou collapse no item correspondente
	 */
	public static function check_menu_ativo($menu)
	{
		$rota_atual = \Request::path();

		// Removemos o action da rota(create, show...)
		$rota_full = explode('/', $rota_atual);
		// agora fica só admin/role por exemplo
		if(count($rota_full) > 2)
		{
			$rota_atual = $rota_full[0].'/'.$rota_full[1];
		}

		// Percorremos o array para verificar em qual posição tem esta rota
		// e então colocar uma classe ou o que precisar:
		foreach($menu as $nome => $dados){
			// se já no menu principal temos o item:
			if( (isset($dados['url']) && ($dados['url'] == $rota_atual)) && (!self::tem_submenu($dados))){
				$menu[$nome]['active'] = true;
				return $menu;
			}
			// Pode estar no submenu:
			if(isset($dados['submenus'])){
				foreach($dados['submenus'] as $subnome => $subdados){
					if(isset($subdados['url']) && ($subdados['url'] == $rota_atual) ){
						// Setamos como rota atual para dar destaque:
						$menu[$nome]['submenus'][$subnome]['active'] = true;
						// informamos que o menu principal precisa de "collapse in"
						$menu[$nome]['collapse'] = true; 
						return $menu;
					}		
				}
			}
		}

		// se nada aconteceu, apenas retorna o menu:
		return $menu;
	}
}






