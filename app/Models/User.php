<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

// logs:
use Spatie\Activitylog\Traits\LogsActivity;

// Permissões:
use Spatie\Permission\Traits\HasRoles;

// Media:
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class User extends Authenticatable implements HasMediaConversions
{
    use Notifiable;
    use HasRoles; // para as permissões
    use LogsActivity;
    use HasMediaTrait;

    /**
     * Aqui informamos o recorte automático desde model
     */
    public function registerMediaConversions()
    {
        // Buscamos do config/imagens as dimensoes:
        foreach(config('imagens.users') as $collection => $formatos){
            foreach($formatos as $formato => $dimensoes){
                // qual formato?
                if( (!isset($dimensoes[2])) || (isset($dimensoes[2]) && empty($dimensoes[2])) ) {
                    $dimensoes[2] = 'jpg';
                }
                $this->addMediaConversion($formato)
                     ->fit('crop', $dimensoes[0], $dimensoes[1])
                     ->format($dimensoes[2])
                     // A opção abaixo estica a imagem se ela for menor que as dimensões informadas:
                     //->fit('stretch', $dimensoes[0], $dimensoes[1])
                     ->optimize()
                     ->nonQueued()
                     ->performOnCollections($collection);
            }
        }
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password', 
        'facebook_id',
    ];

    /**
     * O que vai pro log?
     */
    protected static $logAttributes = ['name', 'email'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Para dar um auto bcrypt ao inserir ou atualizar uma senha de usuário:
     */
    public function setPasswordAttribute($password)
    {   
        // caso a senha já veio criptografada, não faz nada:
        if(strlen($password) < 60)
            $this->attributes['password'] = bcrypt($password);
        else
            $this->attributes['password'] = $password;
    }

    /**
     * Retorna o primeiro nome do usuário
     */
    public function first_name()
    {
        $nome = explode(' ', $this->name);
        if(isset($nome[0]))
            $nome = $nome[0];
        else
            $nome = $this->name;
        return $nome;
    }

    /**
     * Obtém a foto do User
     * Verifica se ele tem facebook
     */
    public function getFoto($formato = 'thumb')
    {
        // Se tem foto do fazebook:
        if(!empty($this->facebook_id)){
            // buscamos no formato desejado:
            foreach(config('imagens.users') as $campos){
                foreach($campos as $nome_campo => $dimensoes){
                    if($formato == $nome_campo){
                        return 'http://graph.facebook.com/'.$this->facebook_id.'/picture?width='.$dimensoes[0].'&height='.$dimensoes[0];
                    }
                }
            }
        }

        // se tem foto via upload:
        $foto = $this->getFirstMediaUrl('user_foto', $formato);
        if($foto)
            return asset($foto);
        return null;
    }

}








