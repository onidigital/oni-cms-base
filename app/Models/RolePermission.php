<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermission extends Model
{

	/**
	 * Aqui recebemos o id de um papel e vinculamos um array de permissoes a ele:
	 * return @void
	 */
	public function processar_permissoes($permissoes, $role_id)
	{
		$role = Role::find($role_id);
        if(!$role)
            return false;

		// Removemos as permissoes desta role que não serão utilizadas:
		$this->vincular_permissoes($permissoes, $role);

		// Atualizamos as permissões, removendo as que o papel não irá utilizar mais:
		$this->atualiza_permissoes($permissoes, $role);
	}

    /**
     * Aqui removemos as permissoes deste papel que não estão no array vindo:
     */
    public function vincular_permissoes($permissoes, $role)
    {
    	$vinculadas = [];
    	foreach($permissoes as $p){
    		// se já não tiver permissão, dá agora:
    		if(!$role->hasPermissionTo($p))
				$role->givePermissionTo($p);
			$vinculadas[] = $p;
		}
    }

    /**
     * Aqui removemos as permissoes que não estão no array recebido
     * o motivo é atualizar as permissões de um papel após atualizar
     */
    public function atualiza_permissoes($array_permissoes, $role)
    {
    	// Passamos pelo array de permissoes existentes:
    	$permissoes = Permission::all();
    	foreach($permissoes as $p){
    		if(!in_array($p->name, $array_permissoes)){
    			$role->revokePermissionTo($p->name);
    		}
    	}
    }

    /**
     * Verifica se existe uma permissao
     */
    public function existe_permissao($acao)
    {
    	$p = Permission::where('name', $acao)->first();
    	if($p == null)
    	{
    		return false;
    	}
    	return true;
    }

}
