<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity; // <-- pra salvar o log

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;

// Para upload e recorte de imagens:
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

/**
 * Configurações padrão do site/admin
 */

class Configuracao extends Model implements HasMediaConversions
{
	protected $table = 'configuracoes';

    use LogsActivity;
    use HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'site_nome',
    	'site_descricao',
    	'site_palavras_chave',
    	'site_publicado',
    	'debug',
		'facebook', 'instagram', 'twitter', 'youtube'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('fields', function (Builder $builder) {
        	// para evitar o select * em todas as queries
            $builder->select('id',
                             'site_nome',
					    	 'site_descricao',
					    	 'site_palavras_chave',
					    	 'site_publicado',
					    	 'debug',
							 'facebook', 'instagram', 'twitter', 'youtube',
                             'created_at', 'updated_at');
        });
    }

    /**
     * Aqui informamos o recorte automático desde model
     */
    public function registerMediaConversions()
    {
        // Buscamos do config/imagens as dimensoes:
        foreach(config('imagens.configuracoes') as $collection => $formatos){
            foreach($formatos as $formato => $dimensoes){
                // qual formato?
                if( (!isset($dimensoes[2])) || (isset($dimensoes[2]) && empty($dimensoes[2])) ) {
                    $dimensoes[2] = 'jpg';
                }
                $this->addMediaConversion($formato)
                     ->fit('crop', $dimensoes[0], $dimensoes[1])
                     ->format($dimensoes[2])
                     // A opção abaixo estica a imagem se ela for menor que as dimensões informadas:
                     //->fit('stretch', $dimensoes[0], $dimensoes[1])
                     ->optimize()
                     ->nonQueued()
                     ->performOnCollections($collection);
            }
        }
    }

    /**
     * Aqui obtemos as configs
     * @return [object]
     */
    public static function getConfigs()
    {
		$c = Self::first();
		// se não existir algum registro, criamos:
		if(!$c){
			 $c = [
	            'site_nome'       => 'Site Onimal',
	         ];
	         $c = Configuracao::create($c);
		}

    	return $c;
    }

    public function logo($formato = 'grande')
    {
        // se tem foto via upload:
        $foto = $this->getFirstMediaUrl('site_logo', $formato);
        if($foto)
            return asset($foto);
        return null;
    }
}
