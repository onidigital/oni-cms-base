<?php

namespace App\Http\Controllers\Site;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\SiteContatoRequest;
use Notification, Session, Sentenca, Redirect;
use App\Models\User;

class ContatoController extends Controller
{
    public function __construct()
    {
    	
    }

    public function index()
    {
    	return view('site.contato');
    }

    /**
     * Aqui enviamos a mensagem de contato:
     */
    public function enviar(SiteContatoRequest $request)
    {
        $input = $request->all();
        $enviou = Notification::send(User::all(), new \App\Notifications\ContatoSite($input));

        Session::flash('alert-info', Sentenca::get('msg_pos_envio_contato_site', 'Mensagem enviada com sucesso'));

        return Redirect::back();
    }
}
