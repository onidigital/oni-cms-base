<?php

namespace App\Http\Controllers\Site;
use App\Http\Controllers\Controller;
use Auth;

class ManutencaoController extends Controller
{
    public function __construct()
    {
    	parent::__construct();
    }

    public function index()
    {
    	
      // se está nesse método mas não estamos em manutenção, manda pra home:
      if($this->configuracoes->site_publicado || Auth::check()){
        return redirect('/');
      }

      return view('site/manutencao', [
      ]);
    }

}
