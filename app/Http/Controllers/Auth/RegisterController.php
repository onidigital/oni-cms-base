<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Socialite;
use Spatie\Permission\Models\Role;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Registramos o usuário pelo Facebook
     *
     * @return Response
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtemos a resposta do Facebook
     *
     * @return Response
     */
    public function handleFacebookCallback()
    {
        // 1 - Verificar se o usuário já está cadastrado com o facebook_id
        // 2 - Se não está, cadastramos
        // 3 - Com o cadastro obtido, logamos o fulano

        try{
            $socialUser = Socialite::driver('facebook')->user();
        }catch(\Exception $e){
            return redirect('/admin');
        }

        $user = User::where('facebook_id', $socialUser->getId())
                      ->where('facebook_id', '!=', null)
                      ->first();
        if(!$user){

            $existe_email = User::where('email', $socialUser->getEmail())->first();
            if($existe_email){
                Session::flash('alert-danger', 'O e-mail "'.$socialUser->getEmail().'" já está cadastrado');
                return redirect()->to('admin/');
            }

            // se não existe, criamos um:
            $user = User::create([
                'facebook_id' => $socialUser->getId(),
                'name'        => $socialUser->getName(),
                'email'       => $socialUser->getEmail(),
            ]);
            // Associa a um papel padrão:
            $role_padrao = $this->getRolePadrao();
            if($role_padrao)
                $user->assignRole($role_padrao->name);
        };

        auth()->login($user);
        return redirect()->to('admin/');
        // $user->token;
    }

    /**
     * Verifica se existe um papel padrão (visitante)
     * retorna o papel
     */
    public function getRolePadrao()
    {
        $role = Role::where('name', 'visitante')->first();
        return $role;
    }
}













