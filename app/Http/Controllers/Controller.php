<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Configuracao;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $configuracoes;

    public function __construct()
    {
    	// Obtemos as configs:
    	$this->configuracoes = Configuracao::getConfigs();

        // Nome do admin:
        config(['app.name' => $this->configuracoes->site_nome ]);

    	// Ativar debugbar?
    	if(isset($this->configuracoes->debug) && ($this->configuracoes->debug > 0)){
            
            // altera o app/config:
            config(['app.debug' => true]);

            // Com Debugbar?
            if($this->configuracoes->debug > 1)
    		  \Debugbar::enable();
            else
              \Debugbar::disable();

    	}else{
            // remove todo debug:
            config(['app.debug' => false]);
    		\Debugbar::disable();
    	}

    	// Compartilhamos com as views:
    	View::share([
    		'configuracoes' => $this->configuracoes,
    	]);
    }
}
