<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session, Redirect, Auth, Response;
use Notification, DB;

class NotificacoesController extends Controller
{
    // Array que guarda variáveis que serão utilizadas nas views:
    public $variaveis;

    public function __construct()
    { 
        parent::__construct();

        // Declarando a variavel como objeto:
        $this->variaveis = (object) null;

        // Caminho a ser utilizado para buscar as views:
        $this->variaveis->pasta = 'admin/notificacoes/';

        // Rota para utilizar no form e redirects:
        $this->variaveis->rota  = 'admin/notificacoes/';

        // Título para a view:
        $this->variaveis->titulo = 'Notificacões';
    }

    /**
     * Lista os itens desta entidade
     */
    public function index()
    {
    	$registros = Auth::user()->notifications;
        return view($this->variaveis->pasta.'index',  
                [ 'registros'  => $registros,
                  'variaveis'  => $this->variaveis
                ]);
    }

    /**
     * Ver/Editar um registro
     */
    public function show($id)
    {
        $r = Auth::user()->notifications->where('id', $id)->first();
        if(!$r){
            Session::flash('alert-danger', config('onicms.mensagens.erro.registro_nao_encontrado') );
            return redirect()->back();
        }

    	return view($this->variaveis->pasta.'show',
                    [ 
                        'registro'   => $r,
                        'variaveis'  => $this->variaveis,
                    ]);
    }

    /**
     * Receber o PUT e atualizar um registro
     */
    public function update(Request $request)
    {
        $input = $request->all();
    	try {
    		$r = Auth::user()->notifications->where('id', $id)->first();
            $r->update($input);
    		Session::flash('alert-success', config('onicms.mensagens.sucesso.update_registro') );

    	}catch (\Exception $e){
    		Session::flash('alert-danger', config('onicms.mensagens.erro.update_registro') );
    	}

        // volta pra tela 'show' ou 'alterar_cadastro', dependendo de onde veio
    	return Redirect::back();
    }

    /**
     * Excluir um registro
     */
    public function destroy($id)
    {
    	try {
    		$r = DB::table('notifications')->where('id', $id)->delete();
    		Session::flash('alert-info', config('onicms.mensagens.sucesso.destroy_registro') );
    	}catch (\Exception $e){
    		Session::flash('alert-danger', config('onicms.mensagens.erro.destroy_registro') );
    	}

        // Volta pra tela de listagem:
    	return redirect($this->variaveis->rota);
    }

    /**
     * Excluir todas lidas
     */
    public function excluir_notificacoes()
    {
    	try {
    		$r = Auth::user()->notifications()->delete();
    		Session::flash('alert-info', 'Todas as suas notificações foram removidas' );
    	}catch (\Exception $e){
    		Session::flash('alert-danger', 'Ocorreu um erro ao remover suas notificações' );
    	}

        // Volta pra tela de listagem:
    	return redirect($this->variaveis->rota);
    }
}
