<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session, Redirect, Auth, Response;
use Onicmspack\Sentencas\Models\Sentenca;
use App\Http\Requests\SentencaRequest;

class SentencasController extends Controller
{
    // Array que guarda variáveis que serão utilizadas nas views:
    public $variaveis;

    public function __construct()
    { 
        parent::__construct();
        
        // Declarando a variavel como objeto:
        $this->variaveis = (object) null;

        // Caminho a ser utilizado para buscar as views:
        $this->variaveis->pasta = 'admin/sentencas/';

        // Rota para utilizar no form e redirects:
        $this->variaveis->rota  = 'admin/sentencas/';

        // Título para a view:
        $this->variaveis->titulo = 'Sentenças utilizadas no site';

        // label para as permissões:
        $this->variaveis->label_permissoes = 'sentencas';
    }

    /**
     * Lista os itens desta entidade
     */
    public function index()
    {
        // Tem permissão?
        if(! Auth::user()->can('listar '.$this->variaveis->label_permissoes)){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

    	$registros = Sentenca::orderBy('slug')->get();
        return view($this->variaveis->pasta.'index',  
                [ 'registros'  => $registros,
                  'variaveis'  => $this->variaveis
                ]);
    }

    /**
     * Criar um registro
     */
    public function create()
    {
        // Tem permissão?
        if(! Auth::user()->can('inserir '.$this->variaveis->label_permissoes)){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

    	return view($this->variaveis->pasta.'create', 
                [ 
                    'variaveis'  => $this->variaveis,
                ]);
    }

    /**
     * Receber o POST e guardar o registro
     */
    public function store(SentencaRequest $request)
    {
        // Tem permissão?
        if(! Auth::user()->can('inserir '.$this->variaveis->label_permissoes)){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        $input = $request->all();

    	try {
    		
            $r = Sentenca::create($input);
    		Session::flash('alert-success', config('onicms.mensagens.sucesso.add_registro') );

    	}catch (\Exception $e){
    		Session::flash('alert-danger', config('onicms.mensagens.erro.add_registro') );
    	}
    	
    	return redirect($this->variaveis->rota.'create');
    }

    /**
     * Ver/Editar um registro
     */
    public function show($id)
    {
        // Tem permissão?
        if( (!Auth::user()->can('editar '.$this->variaveis->label_permissoes))){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        $r = Sentenca::find($id);
        if(!$r){
            Session::flash('alert-danger', config('onicms.mensagens.erro.registro_nao_encontrado') );
            return redirect()->back();
        }

    	return view($this->variaveis->pasta.'show',
                    [ 
                        'registro'   => $r,
                        'variaveis'  => $this->variaveis,
                    ]);
    }

    /**
     * Receber o PUT e atualizar um registro
     */
    public function update(SentencaRequest $request)
    {
        $input = $request->all();

        // Tem permissão?
        if( !Auth::user()->can('editar '.$this->variaveis->label_permissoes) ){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

    	try {
    		$r = Sentenca::find($input['id']);
            $r->update($input);
    		Session::flash('alert-success', config('onicms.mensagens.sucesso.update_registro') );

    	}catch (\Exception $e){
    		Session::flash('alert-danger', config('onicms.mensagens.erro.update_registro') );
    	}

        // volta pra tela 'show' ou 'alterar_cadastro', dependendo de onde veio
    	return Redirect::back();
    }

    /**
     * Excluir um registro
     */
    public function destroy($id)
    {
        // Tem permissão?
        if(! Auth::user()->can('excluir '.$this->variaveis->label_permissoes)){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

    	try {
    		$r = Sentenca::destroy($id);
    		Session::flash('alert-info', config('onicms.mensagens.sucesso.destroy_registro') );
    	}catch (\Exception $e){
    		Session::flash('alert-danger', config('onicms.mensagens.erro.destroy_registro') );
    	}

        // Volta pra tela de listagem:
    	return redirect($this->variaveis->rota);
    }
}
