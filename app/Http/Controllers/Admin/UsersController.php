<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Http\Requests\UserRequest;
use Session, Redirect, Auth, Response;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    // Array que guarda variáveis que serão utilizadas nas views:
    public $variaveis;

    // guarda as opções de papéis de usuários:
    public $roles;

    public function __construct()
    {
        parent::__construct();

        // Declarando a variavel como objeto:
        $this->variaveis = (object) null;

        // Caminho a ser utilizado para buscar as views:
        $this->variaveis->pasta = 'admin/users/';

        // Rota para utilizar no form e redirects:
        $this->variaveis->rota  = 'admin/users/';

        // Título para a view:
        $this->variaveis->titulo = 'Usuários do sistema';

        // papéis de usuários para o select:
        $this->roles = $roles = Role::orderBy('name')->pluck('name', 'name');
    }

    /**
     * Lista os itens desta entidade
     */
    public function index()
    {
        // Tem permissão?
        if(! Auth::user()->can('listar usuarios')){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

    	$users = User::all();
        return view($this->variaveis->pasta.'index',
                [ 'users'      => $users,
                  'variaveis'  => $this->variaveis
                ]);
    }

    /**
     * Criar um registro
     */
    public function create()
    {
        // Tem permissão?
        if(! Auth::user()->can('inserir usuarios')){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

    	return view($this->variaveis->pasta.'create',
                [
                    'variaveis'  => $this->variaveis,
                    'roles'      => $this->roles,
                ]);
    }

    /**
     * Receber o POST e guardar o registro
     */
    public function store(UserRequest $request)
    {
        // Tem permissão?
        if(! Auth::user()->can('inserir usuarios')){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        $input = $request->all();
    	try {

            $user = User::create($input);
    		Session::flash('alert-success', config('onicms.mensagens.sucesso.add_registro') );
            // atualiza o papel do usuário:
            if(isset($input['role']) && (!empty($input['role'])))
                $user->assignRole($input['role']);
            //add foto?
            $this->add_foto($user, $input);

    	}catch (\Exception $e){
    		Session::flash('alert-danger', config('onicms.mensagens.erro.add_registro') );
    	}

    	return redirect($this->variaveis->rota.'create');
    }

    /**
     * Ver/Editar um registro
     */
    public function show($id)
    {
        // Tem permissão?
        if( (!Auth::user()->can('editar usuarios'))){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        $user = User::with('media')->find($id);
        if(!$user){
            Session::flash('alert-danger', config('onicms.mensagens.erro.registro_nao_encontrado') );
            return redirect()->back();
        }

        // obtém o nome do papel do usuário pra ficar "selected" no form:
        $role = $user->roles()->pluck('name')->all();
        if(isset($role[0]))
            $user->role = $role[0];

    	return view($this->variaveis->pasta.'show',
                    [
                        'user'  => $user,
                        'variaveis'  => $this->variaveis,
                        'roles'      => $this->roles,
                    ]);
    }

    /**
     * Receber o PUT e atualizar um registro
     */
    public function update(UserRequest $request)
    {
        $input = $request->all();

        // Tem permissão?
        if( (!Auth::user()->can('editar usuarios')) && ($input['id'] != Auth::user()->id) ){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        if( isset($input['password_new']) && !empty($input['password_new']) )
        {
            if(!isset($input['password_confirmation']))
            {
                Session::flash('alert-danger', 'Precisa informar a confirmação de senha');
                return redirect()->back();
            }

            if($input['password_new'] != $input['password_confirmation'])
            {
                Session::flash('alert-danger', 'Senha de confirmação não corresponde.');
                return redirect()->back();
            }

            $input['password'] = $input['password_new'];
        }

    	try {
    		$user = User::find($input['id']);
            $user->update($input);
    		Session::flash('alert-success', config('onicms.mensagens.sucesso.update_registro') );

            // atualiza o papel do usuário:
            if(isset($input['role']) && (!empty($input['role']))){
                // se o user já tiver essa role, ignora:
                $user = User::find($input['id']);
                $user->syncRoles($input['role']);
            }

            //add foto?
            $this->add_foto($user, $input);

    	}catch (Exception $e){
    		Session::flash('alert-danger', config('onicms.mensagens.erro.update_registro') );
    	}

        // volta pra tela 'show' ou 'alterar_cadastro', dependendo de onde veio
    	return Redirect::back();
    }

    /**
     * Excluir um registro
     */
    public function destroy($id)
    {
        // Tem permissão?
        if(! Auth::user()->can('excluir usuarios')){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

    	try {
    		$user = User::destroy($id);
    		Session::flash('alert-success', config('onicms.mensagens.sucesso.destroy_registro') );
    	}catch (\Exception $e){
    		Session::flash('alert-danger', config('onicms.mensagens.erro.destroy_registro') );
    	}

        // Volta pra tela de listagem:
    	return redirect($this->variaveis->rota);
    }

    /**
     * Para o usuário logado poder alterar seu cadastro
     */
    public function alterar_cadastro()
    {
        return view($this->variaveis->pasta.'show',
                    [
                        'user'      => Auth::user(),
                        'variaveis' => $this->variaveis
                    ]);
    }

    /**
     * Aqui verificamos se tem foto pra add ao user:
     */
    public function add_foto($user, $input)
    {
        if(isset($input['foto'])){
            $user->clearMediaCollection('users');

            $user->addMediaFromRequest('foto')
                 ->toMediaLibrary('user_foto');
        }
    }

    /**
     * Remover uma imagem
     * @param $id do registro em questão
     * @param nome da collection
     */
    public function remover_imagem($registro_id, $mediaItem, $collection = '')
    {
        if(empty($collection)){
            $collection = 'user_foto';
        }
        $registro = User::find($registro_id);
        if($registro){
            // se for pra remover um item de uma collection:
            if($mediaItem > 0){
                $registro->getMedia($collection)[$mediaItem]->delete();
            }else{
                // ou se é para limpar uma collection:
                $registro->getFirstMedia($collection)->delete();
            }
            Session::flash('alert-info', 'Arquivo removido' );
        }else{
            Session::flash('alert-danger', 'Erro ao remover o arquivo');
        }
        return Redirect::back();
    }

    /**
     * Aqui marcamos as notificações de um usuário como lidas:
     */
    public function atualizar_notificacoes()
    {
        Auth::user()->unreadNotifications->markAsRead();
        return Response::json_encode(['success' => true]);
    }
}
