<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use Session, Redirect, Auth;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\RolePermission;

class RolesController extends Controller
{
    // Array que guarda variáveis que serão utilizadas nas views:
    public $variaveis;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    { 
        parent::__construct();
        
        // Declarando a variavel como objeto:
        $this->variaveis = (object) null;

        // Caminho a ser utilizado para buscar as views:
        $this->variaveis->pasta = 'admin/roles/';

        // Rota para utilizar no form e redirects:
        $this->variaveis->rota  = 'admin/roles/';

        // Título para a view:
        $this->variaveis->titulo = 'Papéis de usuários e permissões';
    }

    public function index()
    {
        // Tem permissão?
        if(! Auth::user()->can('listar papeis')){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        $roles = Role::all();
        return view($this->variaveis->pasta.'index', 
                [ 
                    'roles'      => $roles,
                    'variaveis'  => $this->variaveis
                ]
        );
    }

    public function create()
    {
        // Tem permissão?
        if(! Auth::user()->can('inserir papeis')){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        return view($this->variaveis->pasta.'create', 
                [ 
                    'variaveis'  => $this->variaveis,
                    'admin_checked' => '',
                    'admin_disabled' => '',
                ]
        );
    }

    public function store(RoleRequest $request)
    {

        // Tem permissão?
        if(! Auth::user()->can('inserir papeis')){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        $input = $request->all();
        $role['name'] = $input['name'];
        try {
            
            $registro = Role::create($role);
            Session::flash('alert-success', config('onicms.mensagens.sucesso.add_registro', 'Registro adicionado') );
            
            // Salva as permissoes:
            $this->salvar_permissoes($input, $registro->id);

        }catch (\Exception $e){
            Session::flash('alert-danger', config('onicms.mensagens.erro.add_registro', 'Não foi possível adicionar o registro') );
        }
        
        return redirect($this->variaveis->rota.'create');
    }

    public function show(Role $role)
    {
        // Tem permissão?
        if(! Auth::user()->can('editar papeis')){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        // se for o papel de admin, deixa claro que ele tem permissão de tudo:
        $admin_checked  = '';
        $admin_disabled = '';
        if($role->name == 'admin'){
            $admin_checked  = 'checked';
            $admin_disabled = 'disabled';
        }

        return view($this->variaveis->pasta.'show',
                [ 
                    'role'       => $role,
                    'variaveis'  => $this->variaveis,
                    'admin_checked' => $admin_checked,
                    'admin_disabled' => $admin_disabled,
                ]
        );
    }

    public function update(RoleRequest $request)
    {
        // Tem permissão?
        if(! Auth::user()->can('editar papeis')){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        $input = $request->all();
        $role['name'] = $input['name'];
        try {
            $registro = Role::find($request->get('id'))->update($role);
            Session::flash('alert-success', config('onicms.mensagens.sucesso.update_registro', 'Registro atualizado') );

            // Salva as permissoes:
            $this->salvar_permissoes($input, $input['id']);

        }catch (Exception $e){
            Session::flash('alert-danger', config('onicms.mensagens.erro.update_registro', 'Não foi possível atualizar o registro') );
        }
        
        return Redirect::back();
    }

    public function destroy($id)
    {
        // Tem permissão?
        if(! Auth::user()->can('excluir papeis')){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        try {
            $registro = Role::destroy($id);
            Session::flash('alert-success', config('onicms.mensagens.sucesso.destroy_registro', 'Registro excluído') );
        }catch (\Exception $e){
            Session::flash('alert-danger', config('onicms.mensagens.erro.destroy_registro', 'Não foi possível excluir o registro') );
        }

        // Volta pra tela de listagem:
        return redirect($this->variaveis->rota);
    }

    /**
     * Salvar/atualizar as permissões para o papel:
     */
    public function salvar_permissoes($input, $role_id)
    {
        // se desmarcou tudo, colocamos um array vazio:
        if(!isset($input['permissoes']))
            $input['permissoes'] = [];

        $r = new RolePermission;
        // o método vai fazer a bagaça toda:
        $r = $r->processar_permissoes($input['permissoes'], $role_id);
    }
}
