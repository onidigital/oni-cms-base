<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuracao;
use App\Http\Requests\ConfiguracaoRequest;
use Session, Redirect, Auth, Response;
use Illuminate\Support\Facades\Input;

class ConfiguracoesController extends Controller
{
    // Array que guarda variáveis que serão utilizadas nas views:
    public $variaveis;

    public function __construct()
    {
        parent::__construct();
        // Declarando a variavel como objeto:
        $this->variaveis = (object) null;

        // label para as permissões:
        $this->variaveis->label_permissoes = 'configuracoes';

        // Caminho a ser utilizado para buscar as views:
        $this->variaveis->pasta = 'admin/'.$this->variaveis->label_permissoes.'/';

        // Rota para utilizar no form e redirects:
        $this->variaveis->rota  = 'admin/'.$this->variaveis->label_permissoes.'/';

        // Título para a view:
        $this->variaveis->titulo = 'Configurações gerais';

    }

    /**
     * Lista os itens desta entidade
     */
    public function index()
    {
        return $this->show();
    }

    /**
     * Criar um registro
     */
    public function create()
    {
        // Tem permissão?
        if(! Auth::user()->can('inserir '.$this->variaveis->label_permissoes)){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        return view($this->variaveis->pasta.'create',
                [
                    'variaveis'  => $this->variaveis,
                ]);
    }

    /**
     * Receber o POST e guardar o registro
     */
    public function store(ConfiguracaoRequest $request)
    {
        // Tem permissão?
        if(! Auth::user()->can('inserir '.$this->variaveis->label_permissoes)){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        $input = $request->all();

        try {
            $r = Configuracao::create($input);
            Session::flash('alert-success', config('onicms.mensagens.sucesso.add_registro') );

        }catch (Exception $e){
            Session::flash('alert-danger', config('onicms.mensagens.erro.add_registro') );
        }

        return redirect($this->variaveis->rota.'create');
    }

    /**
     * Ver/Editar um registro
     */
    public function show($id = null)
    {
        // Tem permissão?
        if( (!Auth::user()->can('editar '.$this->variaveis->label_permissoes))){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        // pega a config que está sendo chamada no controller principal (Controller.php):
        $r = $this->configuracoes; 

        if(!$r){
            Session::flash('alert-danger', config('onicms.mensagens.erro.registro_nao_encontrado') );
            return redirect()->back();
        }

        return view($this->variaveis->pasta.'show',[
                'registro'   => $r,
                'variaveis'  => $this->variaveis,
        ]);
    }

    /**
     * Receber o PUT e atualizar um registro
     */
    public function update(ConfiguracaoRequest $request)
    {
        $input = $request->all();

        // Tem permissão?
        if( !Auth::user()->can('editar '.$this->variaveis->label_permissoes) ){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        try {
            $r = Configuracao::with('media')->find($input['id']);
            $r->update($input);

            // atualizar imagens?
            $this->add_imagens($r, $input);

            Session::flash('alert-success', config('onicms.mensagens.sucesso.update_registro') );

        }catch (Exception $e){
            Session::flash('alert-danger', config('onicms.mensagens.erro.update_registro') );
        }
        // volta pra tela 'show' ou 'alterar_cadastro', dependendo de onde veio
        return Redirect::back();
    }

    /**
     * Upload de imagens:
     */
    public function add_imagens($registro, $input)
    {
        // Capa
        $campo_img = 'site_logo';
        if( isset($input[$campo_img]) ){
            $file = Input::file($campo_img);
            // limpa a atual:
            $registro->clearMediaCollection('site_logo');
            // add a atual:
            $registro->addMedia($file)
                     ->toMediaLibrary('site_logo');
        }
    }

    /**
     * Remover uma imagem
     * @param $id do registro em questão
     * @param nome da collection
     */
    public function remover_imagem($registro_id, $mediaItem, $collection = '')
    {
        if(empty($collection)){
            $collection = 'site_logo';
        }
        $registro = Configuracao::find($registro_id);
        if($registro){
            // se for pra remover um item de uma collection:
            if($mediaItem > 0){
                $registro->getMedia($collection)[$mediaItem]->delete();
            }else{
                // ou se é para limpar uma collection:
                $registro->getFirstMedia($collection)->delete();
            }
            Session::flash('alert-info', 'Arquivo removido' );
        }else{
            Session::flash('alert-danger', 'Erro ao remover o arquivo');
        }
        return Redirect::back();
    }

}
