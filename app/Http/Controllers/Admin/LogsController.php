<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use Session, Redirect;

use Spatie\Activitylog\Models\Activity;

class LogsController extends Controller
{
    // Array que guarda variáveis que serão utilizadas nas views:
    public $variaveis;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    { 
        parent::__construct();
        // Declarando a variavel como objeto:
        $this->variaveis = (object) null;

        // Caminho a ser utilizado para buscar as views:
        $this->variaveis->pasta = 'admin/logs/';

        // Rota para utilizar no form e redirects:
        $this->variaveis->rota  = 'admin/logs/';

        // Título para a view:
        $this->variaveis->titulo = 'Registro de atividades';
    }

    public function index()
    {
        // Tem permissão?
        if(! Auth::user()->can('listar logs')){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        $logs = Activity::all();
        return view($this->variaveis->pasta.'index', 
                [ 
                    'logs'       => $logs,
                    'variaveis'  => $this->variaveis
                ]
        );
    }

    public function show(Activity $log)
    {
        // Tem permissão?
        if(! Auth::user()->can('editar logs')){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

        return view($this->variaveis->pasta.'show',
                [ 
                    'log'        => $log,
                    'variaveis'  => $this->variaveis
                ]
        );
    }

    /**
     * Excluir um registro
     */
    public function destroy($id)
    {
        // Tem permissão?
        if(! Auth::user()->can('excluir logs')){
            Session::flash('alert-danger', config('onicms.mensagens.erro.nao_autorizado') );
            return redirect()->back();
        }

    	try {
    		$log = Activity::destroy($id);
    		Session::flash('alert-success', config('onicms.mensagens.sucesso.destroy_registro', 'Registro excluído') );
    	}catch (\Exception $e){
    		Session::flash('alert-danger', config('onicms.mensagens.erro.destroy_registro', 'Não foi possível excluir o registro') );
    	}

        // Volta pra tela de listagem:
    	return redirect($this->variaveis->rota);
    }
}
