<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Configuracao;

class checkSitePublicado
{
    public function handle($request, Closure $next, $guard = null)
    {
    	$c = Configuracao::first();
    	// Se o site não foi publicado e não é um usuário logado:
        if ( (!$c->site_publicado) && (!Auth::check()) ) {
            return redirect('/manutencao');
        }

        return $next($request);
    }
}
