<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Notificação para quando um usuário se cadastrar pelo facebook
 */

class CadastroPeloFacebook extends Notification implements ShouldQueue
{
    use Queueable;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $m = new MailMessage();
        $m->line('Novo cadastro de usuário pelo facebook')
          ->line('Nome: '.$this->user->name)
          ->action('Analisar cadastro', url('/admin/users/'.$this->user->id));

        return $m;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'titulo'      => '<strong>'.$this->user->first_name().'</strong> se cadastrou via facebook',
            'user_id'     => $this->user->id,
            'facebook_id' => $this->user->facebook_id,
            'link'        => url('admin/users/'.$this->user->id.''),
        ];
    }
}
