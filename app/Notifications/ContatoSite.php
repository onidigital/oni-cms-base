<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * Notificação para quando um usuário se cadastrar pelo facebook
 */

class ContatoSite extends Notification implements ShouldQueue
{
    use Queueable;
    protected $contato;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($contato)
    {
        $this->contato = $contato;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $m = new MailMessage();
        $m->line('Novo contato do site da Oni')
          ->line('Nome:     '.$this->contato['nome'])
          ->line('E-mail:   '.$this->contato['email'])
          ->line('Telefone: '.$this->contato['telefone'])
          ->line('Mensagem: '.$this->contato['mensagem'])
          ->action('Ver no painel administrativo', url('/admin/notificacoes/'));

        return $m;
    }

    /**
     * Array que será salvo no BD e irá pras notificações:
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'titulo'   => 'Contato do site',
            'nome'     => $this->contato['nome'],
            'email'    => $this->contato['email'],
            'telefone' => $this->contato['telefone'],
            'msg'      => $this->contato['mensagem'],
            'url'      => url('admin/notificacoes/'),
        ];
    }
}
