<!DOCTYPE html>
<html>
<head>
	<title>{{ $configuracoes->site_nome }}</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<style type="text/css">
		body { font-family: arial, verdana }
		.content { margin: 50px auto 0; width: 400px; text-align: center }
	</style>
</head>
<body>

	<div class="content">
		@if($configuracoes->logo())
			<img src="{{ $configuracoes->logo() }}" /> <br>
		@endif
		<h2>{{ Sentenca::get('site_em_manutencao', 'Nosso site está em manutenção.') }}</h2>
	</div>

</body>
</html>