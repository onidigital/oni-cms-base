
@include('site/_partes/_mensagens')

<h1>Formulário de contato</h1>

{!! Form::open(['url' => 'contato']) !!}

	{{ Form::label('nome', 'Nome') }}
	{{ Form::text('nome', null) }} <br>

	{{ Form::label('email', 'E-mail') }}
	{{ Form::text('email', null) }} <br>

	{{ Form::label('telefone', 'Telefone') }}
	{{ Form::text('telefone', null) }} <br>

	{{ Form::label('mensagem', 'Mensagem') }}
	{{ Form::textarea('mensagem', null) }} <br>

	{{ Form::submit('Enviar') }}

{!! Form::close() !!}