<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Oni CMS') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/admin.min.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <div class="wrapper">

            <div class="sidebar" data-active-color="purple" data-background-color="white">
                <div class="logo">
                    <a href="{{ url('/admin') }}" class="simple-text logo-normal" >
                        <img src="{{ asset('img/marca-oni-roxo.svg') }}" alt="Marca Oni" height="40" id="logo-oni">
                    </a>
                </div>

                <div class="sidebar-wrapper">
                    <div class="user">
                        <div class="info">
                            @if( Auth::user()->getFoto() !== NULL)
                                <div class="photo">
                                    <img src="{{ Auth::user()->getFoto() }}" alt="{{ Auth::user()->first_name() }}">
                                </div>
                            @endif
                            @if (Auth::guest())
                                <!-- <li><a href="{{ url('/login') }}">Login</a></li> -->
                            @else
                                <a href="#collapseUser" class="dropdown-toggle" data-toggle="collapse" role="button" aria-expanded="false">
                                    <span>
                                        {{ Auth::user()->name }} <b class="caret"></b>
                                    </span>
                                </a>

                                <div class="collapse" id="collapseUser" aria-expanded="false">
                                    <ul class="nav">
                                        <li>
                                            <a href="{{ url('/admin/users/cadastro') }}">
                                                <span class="sidebar-mini">MC</span>
                                                <span class="sidebar-normal">Meu Cadastro</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/logout') }}"
                                                onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                         <span class="sidebar-mini">OFF</span>
                                                <span class="sidebar-normal">Logout</span>
                                            </a>
                                        </li>
                                        <li>
                                            <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                    @include('admin/_partes/sidebar')
                </div>

                <div class="sidebar-background" style="background-image: url('{{ asset('img/poligonos.svg') }}');"></div>
            </div>{{-- /Siderbar --}}

            <div class="main-panel">
                @include('admin/_partes/navbar')
                @include('admin/_partes/mensagens')
                <div class="content">
                    <div class="container-fluid">
                         @yield('content')
                    </div>
                </div>
                @include('admin/_partes/footer')
            </div>

        </div>

    </div>
    <!-- /#wrapper -->

        
    </div>

    {{-- Scripts --}}
    <script src="{{ asset('js/admin.js') }}"></script>


</body>
</html>
