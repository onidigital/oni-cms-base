@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        {{ $variaveis->titulo }} 
                        <a href="{{ url($variaveis->rota) }}">Consultar</a>
                    </h4>
                </div>

                <div class="card-content">
                    <div class="clearfix">
                    	{!! Form::open(['url' => url($variaveis->rota), 'class' => 'form-horizontal']) !!}
                    		@include($variaveis->pasta.'form')
                    	   {{ Form::submit(config('onicms.labels.botoes.botao_salvar', 'Inserir'), ['class' => 'btn btn-info pull-right']) }}
                    	{!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection