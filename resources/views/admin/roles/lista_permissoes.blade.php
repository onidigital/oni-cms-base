
<!--
	Aqui listamos as permissões pro cara associar ao papel:
	Elas são inseridas em config/permissoes.php
-->
<div class="form-group">
	<label class="col-sm-2 control-label">Permissões</label>

	<div class="col-sm-10">
		<div class="text-right">
			<div class="checkbox">
				<input type="checkbox" id="marcar_todos" {{ $admin_checked }} {{ $admin_disabled }} >
				<label for="marcar_todos">
					Marcar todos
				</label>
			</div>
		</div>

		@forelse(config('permissoes.itens') as $nome => $acoes)
			<div class="col-sm-4">
				<label>{{ $nome }}</label>
				@foreach($acoes as $key => $acao)
					<div class="checkbox">
						<input type="checkbox" id="{{ $acao }}" class="marcar-todos" name="permissoes[]" value="{{ $acao }}"  {{ $admin_checked }} {{ $admin_disabled }}
							@if( isset($role) && $role->hasPermissionTo($acao)) checked @endif > 
						<label for="{{ $acao }}">
							{{ $key }}
						</label>
					</div>
				@endforeach
			</div>
		@empty
			<p>Nenhuma permissão encontrada em config/permissoes.php</p>
		@endif
	</div>
</div>