
{{ Form::hidden('id', null) }}
<div class="form-group">
	{{ Form::label('name', 'Nome', ['class' => 'col-sm-2 control-label']) }}
	<div class="col-sm-10">
		{{ Form::text('name', null, ['class' => 'form-control', 'autofocus']) }}
	</div>
</div>
@include('admin/roles/lista_permissoes')
