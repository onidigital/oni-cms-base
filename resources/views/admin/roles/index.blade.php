@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Lista de papéis</h4>
                    <a href="{{ url($variaveis->rota.'create') }}" class="btn btn-info pull-right btn-acao" title="Cadastrar Novo">
                        <i class="ti-plus"></i> Novo
                    </a>
                </div>

                <div class="card-content">
                	<table class="table"> 
                		<thead>
                			<th>Papel</th>
                			<th class="text-right">Ações</th>
                		</thead>
	                    @foreach($roles as $registro)
		                    <tr>
		                    	<td>{{ $registro->name }}</td>
		                    	<td class="td-actions text-right">
		                    		@include('admin/_partes/botao_editar')
                                    @include('admin/_partes/botao_excluir')
		                    	</td>
		                    </tr>
	                    @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
