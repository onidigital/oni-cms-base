
{{ Form::hidden('id', null) }}
<div class="row">

	<div class="col-sm-10">

		<!-- Se for do tipo contato do site: -->
		@if($registro->type == 'App\Notifications\ContatoSite')
			<p>Nome: {{ $registro->data['nome'] }} </p>
			<p>E-mail: {{ $registro->data['email'] }} </p>
			<p>Telefone: {{ $registro->data['telefone'] }} </p>
			<p>Mensagem: {{ $registro->data['msg'] }} </p>
		@endif
	
		<!-- Se for do tipo cadastro pelo fb: -->
		@if($registro->type == 'App\Notifications\CadastroPeloFacebook')
			<p>{!! $registro->data['titulo'] !!}</p>
			<p><a href="{{ $registro->data['link'] }}" target="_blank" title="ver cadastro">ver cadastro</a></p>
		@endif

		<!-- Se for do tipo Novo anúncio: -->
		@if($registro->type == 'App\Notifications\NovoAnuncio')
			<p>{!! $registro->data['titulo'] !!}</p>
			<p><a href="{{ $registro->data['link'] }}" target="_blank" title="ver cadastro">ver cadastro</a></p>
		@endif
		
		<!-- enviada em -->
		<p> {{ \Carbon\Carbon::parse($registro->created_at)->diffForHumans() }} </p>
	</div>
</div>