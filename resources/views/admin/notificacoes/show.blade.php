@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        {{ $registro->type }}
                    </h4>
                </div>

                <div class="card-content">
                	{!! Form::model($registro, ['url' => [$variaveis->rota.$registro->id], 'method' => 'PUT', 'class' => 'form-horizontal', 'files' => true] ) !!}
                		@include($variaveis->pasta.'form')
                	{!! Form::close() !!}
                    <!-- para ir pra rota de listagem -->
                    <a href="{{ url($variaveis->rota) }}" class="btn btn-info pull-right btn-acao" title="voltar para as notificações">
                        <i class="material-icons">arrow_back</i> Voltar
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection