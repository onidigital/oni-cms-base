@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        {{ $variaveis->titulo }}
                    </h4>
                </div>

                <div class="card-content">
                    @if(count($registros) > 0)
                        <a href="{{ url($variaveis->rota.'limpar_notificacoes') }}" onclick="return confirm('Tem certeza?')" 
                           class="btn btn-warning pull-right btn-acao" 
                           title="excluir todas as notificações">
                            <i class="ti-close"></i> Limpar notificações
                        </a>
                    @endif
                	<table class="table"> 
                		<thead>
                            <th>Data</th>
                			<th>Tipo</th>
                            <th>Lida</th>
                			<th class="text-right">Ações</th>
                		</thead>
	                    @foreach($registros as $registro)
		                    <tr>
		                    	<td>{{ \Carbon\Carbon::parse($registro->created_at)->diffForHumans() }}</td>
                                <td>{{ $registro->type }}</td>
                                <td>{{ $registro->read_at }}</td>
		                    	<td class="td-actions text-right">
                                    @include('admin/_partes/botao_ver')
                                    @include('admin/_partes/botao_excluir')
		                    	</td>
		                    </tr>
	                    @endforeach
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
