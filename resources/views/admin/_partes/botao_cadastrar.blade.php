<a href="{{ url($variaveis->rota.'create') }}" class="btn btn-info pull-right btn-acao" title="{{ config('onicms.labels.botao_cadastrar', 'Novo') }}">
    <i class="ti-plus"></i> {{ config('onicms.labels.botao_cadastrar', 'Novo') }}
</a>