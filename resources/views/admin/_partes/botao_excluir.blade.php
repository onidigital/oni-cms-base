{!! Form::open(['method' => 'DELETE', 'url'=> $variaveis->rota.$registro->id, 'class' => "form-inline"]) !!}
	<button type="submit" onclick="return confirm('{{ config('onicms.alertas.excluir_registro') }}')" title="Excluir este registro" class="btn btn-danger btn-acao">
		<i class="ti-trash"></i>
	</button>
{!! Form::close() !!}