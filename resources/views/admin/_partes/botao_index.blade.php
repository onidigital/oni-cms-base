<!-- para ir pra rota de listagem -->
<a href="{{ url($variaveis->rota) }}" class="btn btn-default pull-right btn-acao" title="{{ config('onicms.labels.botao_listar', 'Cancelar/voltar') }}">
    <i class="ti-close"></i> {{ config('onicms.labels.botao_listar', 'Cancelar') }}
</a>