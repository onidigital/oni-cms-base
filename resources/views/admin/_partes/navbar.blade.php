<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-fill btn-icon"><i class="ti-more-alt"></i></button>
        </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"> 
                @if(isset($variaveis))
                    {{ $variaveis->titulo }} 
                @else
                    {{ config('onicms.titulo_painel') }}
                @endif
            </a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- <li>
                    <a href="/admin" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="material-icons">dashboard</i>
                        <p class="hidden-lg hidden-md">Painel de Controle</p>
                    </a>
                </li> -->
                <li class="dropdown">
                    <?php $total_notificacoes = Auth::user()->unreadNotifications()->count(); ?>
                    @if( !($total_notificacoes > 0) )
                        {{-- se não temos notificações, o botão vira link pra ver as notificações antigas: --}}
                        <a href="{{ url('admin/notificacoes') }}" title="ver notificações passadas">
                    @else
                        <a href="#" id="link-notificacoes" data-url="{{ url('admin/users/atualizar_notificacoes' ) }}" class="dropdown-toggle" data-toggle="dropdown">
                    @endif
                        <i class="ti-bell"></i>
                        @if($total_notificacoes > 0)
                            <span class="notification">{{ $total_notificacoes }}</span>
                        @endif
                        <p class="hidden-lg hidden-md">
                            Notificações
                            <b class="caret"></b>
                        </p>

                    </a>
                    <ul class="dropdown-menu">
                        <?php $notificacoes = Auth::user()->unreadNotifications()->get(); ?>
                        @forelse($notificacoes as $n)
                            @if($n->type == 'App\Notifications\ContatoSite')
                                <li>
                                    <a href="{{ url('admin/notificacoes/'.$n->id) }}">Contato de {{ $n->data['nome'] }}</a>
                                </li>
                            @endif
                            @if($n->type == 'App\Notifications\CadastroPeloFacebook')
                                <li>
                                    <a href="{{ url('admin/notificacoes/'.$n->id) }}">{!! $n->data['titulo'] !!}</a>
                                </li>
                            @endif
                        @empty
                            <li>
                                <a href="javascript:void(0)">sem notificações :(</a>
                            </li>
                        @endforelse
                    </ul>
                </li>
                <!-- <li class="">
                    <a href="#perfil" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <i class="material-icons">person</i>
                        <p class="hidden-lg hidden-md">Perfil</p>
                    <div class="ripple-container"></div></a>
                </li> -->
                <li class="separator hidden-lg hidden-md"></li>
            </ul>
        </div>
    </div>
</nav>