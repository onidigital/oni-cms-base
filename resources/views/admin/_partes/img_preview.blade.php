@if($imagem)
	<a href="{{ $imagem }}" target="_blank">
	    <div class="fileinput-new thumbnail img-circle">
	    	<img src="{{ $thumb }}" alt="{{ $thumb }}" />
	    </div>
	</a>
    <!-- Botão pra remover a imagem -->
    <br>
    <a href="{{ url( $url_prefix.'/remover_imagem/'.$registro_id.'/'.$collection) }}" onclick="return confirm('Tem certeza?')" class="btn btn-danger btn-round"><i class="fa fa-times"></i> Remover Imagem</a>
	<!-- Botão pra remover a imagem -->
@endif