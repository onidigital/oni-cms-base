@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ $variaveis->titulo }}</h4>
                </div>

                <div class="card-content">
                
                	<table class="table"> 
                		<thead>
                			<th>Nome</th>
                            <th>Descrição</th>
                			{{-- <th class="text-right">Ações</th> --}}
                		</thead>
	                    @foreach($logs as $registro)
		                    <tr>
		                    	<td>{{ $registro->subject_type }}</td>
                                <td>{{ $registro->description }}</td>
		                    	{{-- <td class="td-actions text-right">
		                    		@include('admin/_partes/botao_editar')
                                    @include('admin/_partes/botao_excluir')
		                    	</td> --}}
		                    </tr>
	                    @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
