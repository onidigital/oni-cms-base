
{{ Form::hidden('id', null) }}
<ul>
	<li>
		{{ Form::label('name', 'Nome')}}
		{{ Form::text('name', null, ['class' => '', 'autofocus']) }}
	</li>
	<li>
		{{ Form::label('email', 'E-mail')}}
		{{ Form::email('email', null, ['class' => '']) }}
	</li>
	<li>
		{{ Form::label('role', 'Papel')}}
		{{ Form::select('role', $roles, null, ['class' => '']) }}
	</li>
	@if(!isset($user))
		<li>
			{{ Form::label('password', 'Senha')}}
			{{ Form::password('password', null, ['class' => '']) }}
		</li>
		<li>
			{{ Form::label('password_confirmation', 'Confirmar senha')}}
			{{ Form::password('password_confirmation', null, ['class' => '']) }}
		</li>
	@endif
</ul>