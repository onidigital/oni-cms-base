@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $user->name or 'Editar'}} <a href="{{ url($variaveis->rota) }}">Consultar</a></div>

                <div class="panel-body">
                	{!! Form::model($user, ['url' => [$variaveis->rota.$user->id], 'method' => 'PUT' ]) !!}
                		@include($variaveis->pasta.'form')
                	{{ Form::submit(config('onicms.labels.botoes.botao_atualizar', 'Atualizar')) }}
                	{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection