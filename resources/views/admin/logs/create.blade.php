@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Usuários <a href="{{ url($variaveis->rota) }}">Consultar</a></div>

                <div class="panel-body">
                	{!! Form::open(['url' => $variaveis->rota]) !!}
                		@include($variaveis->pasta.'form')
                	{{ Form::submit(config('onicms.labels.botoes.botao_salvar', 'Inserir')) }}
                	{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection