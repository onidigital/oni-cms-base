@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Lista de Sentenças
                    </h4>
                    @include('admin/_partes/botao_cadastrar')
                </div>

                <div class="card-content">
                	<table class="table"> 
                		<thead>
                            <th>Slug</th>
                			<th>Valor</th>
                            <th>Descrição</th>
                			<th class="text-right">Ações</th>
                		</thead>
	                    @foreach($registros as $registro)
		                    <tr>
		                    	<td>{{ $registro->slug }}</td>
                                <td>{{ $registro->valor }}</td>
                                <td>{{ $registro->descricao }}</td>
		                    	<td class="td-actions text-right">
	                    		    @include('admin/_partes/botao_editar')
                                    @include('admin/_partes/botao_excluir')
		                    	</td>
		                    </tr>
	                    @endforeach
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
