@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        {{ $registro->nome or 'Editar'}}
                    </h4>
                </div>

                <div class="card-content">
                    <div class="clearfix">
                    	{!! Form::model($registro, ['url' => [$variaveis->rota.$registro->id], 'method' => 'PUT', 'class' => 'form-horizontal', 'files' => true] ) !!}
                    		@include($variaveis->pasta.'form')
                    	{{ Form::submit(config('onicms.labels.botoes.botao_atualizar', 'Atualizar'), ['class' => 'btn btn-warning pull-right']) }}
                    	{!! Form::close() !!}
                        @include('admin/_partes/botao_cadastrar')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection