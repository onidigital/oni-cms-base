{{ Form::hidden('id', null) }}
<div class="form-group">
	{{ Form::label('slug', 'Slug *', ['class' => 'col-sm-2 control-label'])}}
	<div class="col-sm-10">
			{{ Form::text('slug', null, ['class' => 'form-control', 'autofocus']) }}
	</div>
</div>

<div class="form-group">
	{{ Form::label('valor', 'Valor *', ['class' => 'col-sm-2 control-label']) }}
	<div class="col-sm-10">
			{{ Form::text('valor', null, ['class' => 'form-control']) }}
	</div>
</div>

<div class="form-group">
	{{ Form::label('descricao', 'Descrição', ['class' => 'col-sm-2 control-label']) }}
	<div class="col-sm-10">
			{{ Form::text('descricao', null, ['class' => 'form-control']) }}
	</div>
</div>