@extends('layouts.login')

@section('content')
<div class="content">
    <div class="container">  
        <div class="row">
            <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                <div class="login-content">
                    <img src="{{ asset('img/marca-oni-branco.svg') }}" alt="Marca Oni" width="140" height="65" id="logo-oni">
                    <form role="form" method="POST" action="{{ url('/admin/login') }}">
                        {{ csrf_field() }}
                        
                        <div class="card card-login">
                            <div class="card-header text-center">
                                <h4 class="card-title">
                                    Eaí... tá afim de entrar no {{ config('app.name', 'Oni CMS') }}?
                                </h4>
                            </div>
                            <div class="card-content">                    
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="control-label">E-mail</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                </div>
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="control-label">Senha</label>
                                    <input id="password" type="password" class="form-control" name="password" required>
                                </div>
                            </div>
                            <div class="footer text-center">
                                <button type="submit" class="btn btn-roxo btn-lg btn-block">Entrar</button>
                                <a href="{{ url('admin/social/fb') }}" class="btn btn-social btn-fill btn-facebook btn-block">
                                    <i class="fa fa-facebook-square"></i> Entrar com o Facebook
                                    <div class="ripple-container"></div>
                                </a>
                                <br>
                                <a class="" href="{{ route('password.request') }}">
                                    Esqueci minha senha
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
