{{ Form::hidden('id', null) }}
<div class="form-group">
	{{ Form::label('site_nome', 'Nome do site *', ['class' => 'col-sm-3 control-label'])}}
	<div class="col-sm-9">
			{{ Form::text('site_nome', null, ['class' => 'form-control', 'autofocus']) }}
	</div>
</div>

<div class="form-group">
	{{ Form::label('site_descricao', 'Descrição do site', ['class' => 'col-sm-3 control-label']) }}
	<div class="col-sm-9">
			{{ Form::text('site_descricao', null, ['class' => 'form-control']) }}
	</div>
</div>

<div class="form-group">
	{{ Form::label('site_palavras_chave', 'Palavras-chave do site', ['class' => 'col-sm-3 control-label']) }}
	<div class="col-sm-9">
			{{ Form::text('site_palavras_chave', null, ['class' => 'form-control']) }}
	</div>
</div>

<div class="form-group">
	{{ Form::label('facebook', 'Facebook', ['class' => 'col-sm-3 control-label']) }}
	<div class="col-sm-9">
			{{ Form::text('facebook', null, ['class' => 'form-control']) }}
	</div>
</div>

<div class="form-group">
	{{ Form::label('instagram', 'Instagram', ['class' => 'col-sm-3 control-label']) }}
	<div class="col-sm-9">
			{{ Form::text('instagram', null, ['class' => 'form-control']) }}
	</div>
</div>

<div class="form-group">
	{{ Form::label('twitter', 'Twitter', ['class' => 'col-sm-3 control-label']) }}
	<div class="col-sm-9">
			{{ Form::text('twitter', null, ['class' => 'form-control']) }}
	</div>
</div>

<div class="form-group">
	{{ Form::label('youtube', 'Youtube', ['class' => 'col-sm-3 control-label']) }}
	<div class="col-sm-9">
			{{ Form::text('youtube', null, ['class' => 'form-control']) }}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 control-label"> Publicar site? {!! tooltip(config('onicms.help_text.configuracoes.site_publicado')) !!} </label>
	<div class="col-sm-9">
			{{ Form::select('site_publicado', ['0' => 'Não', '1' => 'Sim'], null, ['class' => 'form-control']) }}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 control-label"> Ativar modo debug? {!! tooltip(config('onicms.help_text.configuracoes.debug')) !!} </label>
	<div class="col-sm-9">
		{{ Form::select('debug', ['0' => 'Não', '1' => 'Sim, sem debugbar', '2' => 'Sim, com debugbar'], null, ['class' => 'form-control']) }}
	</div>
</div>

<div class="form-group">
	<label class="col-sm-3 control-label">Logo</label>
	<div class="col-sm-9">
		<div class="fileinput fileinput-new text-center" data-provides="fileinput">

			{{-- aqui mostra o preview da img cadastrada, se houver uma --}}
			@include('admin/_partes/img_preview', [
				'imagem' 	  => $registro->logo('grande'),
				'thumb' 	  => $registro->logo('thumb'),
				'registro_id' => $registro->id,
				'collection'  => 'site_logo',
				'url_prefix'  => 'admin/configuracoes'
			])

			<div class="fileinput-preview fileinput-exists thumbnail img-circle"></div>
		    <div>
		        <span class="btn btn-round btn-primary btn-file">
		            <span class="fileinput-new">Enviar Imagem</span>
		            <span class="fileinput-exists">Mudar</span>
		            <input type="file" name="site_logo" />
		        </span>
		    </div>
		</div>
	</div>
</div>
