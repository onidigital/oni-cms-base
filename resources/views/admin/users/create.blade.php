@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Inserir novo usuário</h4>
                </div>
                 {{-- <a href="{{ url($variaveis->rota) }}">Consultar</a> --}}
                <div class="card-content">
                	{!! Form::open(['url' => $variaveis->rota, 'class' => 'form-horizontal', 'files' => true]) !!}
                		@include($variaveis->pasta.'form')
                	{{ Form::submit(config('onicms.labels.botoes.botao_salvar', 'Inserir'), ['class' => 'btn btn-info pull-right']) }}
                	{!! Form::close() !!}
                    @include('admin/_partes/botao_index')
                </div>
            </div>
        </div>
    </div>
@endsection