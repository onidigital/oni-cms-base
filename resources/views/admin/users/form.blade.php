
{{ Form::hidden('id', null) }}

<div class="form-group">
	{{ Form::label('name', 'Nome', ['class' => 'col-sm-2 control-label'])}}
	<div class="col-sm-10">
		{{ Form::text('name', null, ['class' => 'form-control', 'autofocus']) }}
	</div>
</div>

<div class="form-group">
	{{ Form::label('email', 'E-mail', ['class' => 'col-sm-2 control-label']) }}
	<div class="col-sm-10">
		{{ Form::email('email', null, ['class' => 'form-control']) }}
	</div>
</div>

@if(isset($roles))
	<div class="form-group">
		{{ Form::label('role', 'Papel', ['class' => 'col-sm-2 control-label']) }}
		<div class="col-sm-10">
			{{ Form::select('role', $roles, null, ['class' => 'selectpicker', 'data-style' => 'btn btn-primary btn-round']) }}
		</div>
	</div>
@endif

@if(!isset($user))
	<div class="form-group">
		{{ Form::label('password', 'Senha', ['class' => 'col-sm-2 control-label']) }}
		<div class="col-sm-10">
			{{ Form::password('password', ['class' => 'form-control']) }}
		</div>
	</div>
	<div class="form-group">
		{{ Form::label('password_confirmation', 'Confirmar senha', ['class' => 'col-sm-2 control-label']) }}
		<div class="col-sm-10">
			{{ Form::password('password_confirmation', ['class' => 'form-control']) }}
		</div>
	</div>
@endif

@if(isset($user))
	<div class="form-group">
		{{ Form::label('password_new', 'Alterar Senha', ['class' => 'col-sm-2 control-label']) }}
		<div class="col-sm-10">
			{{ Form::password('password_new', ['class' => 'form-control']) }}
		</div>
	</div>
	<div class="form-group">
		{{ Form::label('password_confirmation', 'Confirmar senha', ['class' => 'col-sm-2 control-label']) }}
		<div class="col-sm-10">
			{{ Form::password('password_confirmation', ['class' => 'form-control']) }}
		</div>
	</div>
@endif

<div class="form-group">
	<label class="col-sm-2 control-label">Imagem</label>
	<div class="col-sm-10">
		<div class="fileinput fileinput-new text-center" data-provides="fileinput">

		    {{-- aqui mostra o preview da img cadastrada, se houver uma --}}
			@include('admin/_partes/img_preview', [
				'imagem' 	  => $user->getFoto('media'),
				'thumb' 	  => $user->getFoto('thumb'),
				'registro_id' => $user->id,
				'collection'  => 'user_foto',
				'url_prefix'  => 'admin/users'
			])

		    @if(isset($user) && $user->facebook_id > 0)
    			<p>(via facebook)</p>
    		@endif

			@if( (isset($user)) && !($user->facebook_id > 0) )

			    <div class="fileinput-preview fileinput-exists thumbnail img-circle"></div>
			    <div>
			        <span class="btn btn-round btn-primary btn-file">
			            <span class="fileinput-new">Enviar Imagem</span>
			            <span class="fileinput-exists">Mudar</span>
			            <input type="file" name="foto" />
			        </span>
			    </div>
			@endif
		</div>
	</div>
</div>
