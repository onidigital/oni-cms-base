@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        Lista de Usuários
                    </h4>
                    @include('admin/_partes/botao_cadastrar')
                </div>

                <div class="card-content">
                	<table class="table"> 
                		<thead>
                			<th>Nome</th>
                			<th class="text-right">Ações</th>
                		</thead>
	                    @foreach($users as $registro)
		                    <tr>
		                    	<td>{{ $registro->name }}</td>
		                    	<td class="td-actions text-right">
	                    		    @include('admin/_partes/botao_editar')
                                    @include('admin/_partes/botao_excluir')
		                    	</td>
		                    </tr>
	                    @endforeach
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
