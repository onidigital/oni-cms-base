@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        {{ $user->name or 'Editar'}}<a href="{{ url($variaveis->rota) }}">Consultar</a>
                    </h4>
                </div>

                <div class="card-content">
                	{!! Form::model($user, ['url' => [$variaveis->rota.$user->id], 'method' => 'PUT', 'class' => 'form-horizontal'] ) !!}
                		@include($variaveis->pasta.'form')
                	{{ Form::submit(config('onicms.labels.botoes.botao_atualizar', 'Atualizar'), ['class' => 'btn btn-warning pull-right']) }}
                	{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection