@extends('layouts.login')

@section('content')
<div class="content">
    <div class="container">  
        <div class="row">
            <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                <div class="login-content">
                    <img src="{{ asset('img/marca-oni-branco.svg') }}" alt="Marca Oni" width="140" height="65" id="logo-oni">
                    <form role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}
                        <div class="card card-login">
                            <div class="card-header text-center">
                                <h4 class="card-title">
                                    Esqueci a senha
                                </h4>
                            </div>
                            <div class="card-content">                    
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label class="control-label">E-mail</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                </div>
                            </div>
                            <div class="footer text-center">
                                <button type="submit" class="btn btn-roxo btn-lg btn-block">Enviar link para nova senha</button>
                                <br>
                                <a href="{{ url('/admin') }}" title="voltar">
                                    Voltar
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
