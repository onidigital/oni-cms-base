$(function(){
    // Máscaras
	$('input[name="cep"], .mask_cep').mask("99999-999")
    $('input[name="cpf"], input[data-mask="cpf"]').mask('999.999.999-99')
    $('.mask_hora').mask("99:99:99")
    $('.mask_telefone, input[name="telefone"], input[name="celular"], input[name="whatsapp"]').focusout(function(){
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if(phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    }).trigger('focusout');

	// quando clicar para marcar todos os checkboxes:
	$('#marcar_todos').click(function(event) {
		selecionado = this.checked;
        // passa por todos checkboxes com a classe abaixo:
        $('.marcar-todos').each(function() {
            this.checked = selecionado;                        
        });
	});

	// Tooltip
    $('.btn-acao').tooltip({
        placement: 'top'
    });

    $('.help-tooltip').tooltip({
        placement: 'top'
    });

    $('*[data-toggle="tooltip"]').tooltip();

    // Ao clicar em notificações, marcar todas como não lidas:
    $('#link-notificacoes').click(function(){
        var url = $(this).attr('data-url');
        if(url.length > 0)
          executar_ajax(url);
    });

    // Editor de texto
    $('.editor').trumbowyg({
        fullscreenable: false,
        removeformatPasted: true,
        lang:'pt',
        btns: ['viewHTML','|', 'formatting','|', 'btnGrp-design','|', 'link','|', 'insertImage','|', 'btnGrp-justify','|', 'btnGrp-lists','|', 'horizontalRule']
    });
});

// Função para acessar o controller de notificações e marcar todas como lidas, em ajax:
function executar_ajax(url)
{
    $.ajax({
      url: url
    });
}