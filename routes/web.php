<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

// Rotas de "Esqueci a senha":
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
// ----------

Route::get('/home', function () {
	// se não tem front, manda pro admin:
    return redirect('/admin');
});

// Rotas de login:
Route::get('admin/login',   'Auth\LoginController@getLogin');
Route::post('admin/login',  'Auth\LoginController@login');
Route::post('admin/logout', 'Auth\LoginController@logout');
Route::get('admin/logout', 'Auth\LoginController@logout');

Route::get('admin/social/fb', 'Auth\RegisterController@redirectToFacebook');
Route::get('admin/social/fb/callback', 'Auth\RegisterController@handleFacebookCallback');


// Rotas do admini:
Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function () {

	Route::get('/', 'Admin\HomeController@index');

	// Alterar cadastro:
	Route::get('users/cadastro', 'Admin\UsersController@alterar_cadastro');

	// para marcar as notificacoes como lidas:
	Route::get('users/atualizar_notificacoes', 'Admin\UsersController@atualizar_notificacoes');

	// Remover a foto do usuário:
	Route::get('users/remover_foto/{id}/{collection}', 'Admin\UsersController@remover_foto');

	// Users:
	Route::resource('users', 'Admin\UsersController');
	Route::get('users/remover_imagem/{registro_id}/{collection}', 'Admin\UsersController@remover_imagem');

	// Roles/papéis
	Route::resource('roles', 'Admin\RolesController');

	// Logs - registro de atividades
	Route::resource('logs', 'Admin\LogsController');

	// Sentenças
	Route::resource('sentencas', 'Admin\SentencasController');

	// Configuraçoes gerais
	Route::resource('configuracoes', 'Admin\ConfiguracoesController');
	Route::get('configuracoes/remover_imagem/{registro_id}/{collection}', 'Admin\ConfiguracoesController@remover_imagem');

	// Notificações
	Route::get('notificacoes/limpar_notificacoes', 'Admin\NotificacoesController@excluir_notificacoes');
	Route::resource('notificacoes', 'Admin\NotificacoesController');

});

// Site:
Route::group(['middleware' => ['web', 'checkSitePublicado']], function(){
	Route::get('/', 'Site\HomeController@index');
	Route::get('contato', 'Site\ContatoController@index');
	Route::post('contato', 'Site\ContatoController@enviar');
});
// Quando o site estiver em manutenção, cai nessa rota:
Route::get('/manutencao', 'Site\ManutencaoController@index');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
