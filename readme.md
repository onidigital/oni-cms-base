Bem-vindo!
===================
Este projeto foi orgulhosamente desenvolvido pela [Oni](https://www.onidigital.com).
Utilizandamos:
 
 - [Laravel 5.4](https://packagist.org/packages/laravel/laravel): [![Latest Stable Version](https://poser.pugx.org/laravel/laravel/v/stable?format=flat)](https://packagist.org/packages/laravel/laravel)
 
 - E nosso CMS [OniCMS](https://packagist.org/packages/onidigital/onicms): [![Latest Stable Version](https://poser.pugx.org/onidigital/onicms/v/stable?format=flat)](https://packagist.org/packages/onidigital/onicms)

----------

## Instalação:
1. **Crie a pasta do projeto no seu ambiente**
2. `$ composer create-project onidigital/onicms pasta_projeto --stability dev`
3. `$ sudo chmod -R 777 storage/ bootstrap/`
4. **Criar o banco e configurar seu .env**
5. `$ composer install`
6. `$ sudo npm install`
7. `$ bower install`
8. `$ php artisan key:generate`
9. `$ php artisan migrate`
10. `$ php artisan db:seed`
11. `$ npm run dev`

> ** Login com o Facebook **
> Para utilizar o login com o FB, deve-se criar o app no facebook (https://developers.facebook.com/apps/), colocar o app publico e colocar sua url de desenvolvimento nas urls válidas de acesso.
> Também alterar os dados no seu .env

> **Importante:**

> Para não dar erro de permissão, o comando de criar projeto não pode ser com sudo, senão as pastas ficam com root como proprietário e dá um monte de erro nos updates

----------
## Configurou seu ambiente? Pronto!

> **Nota:**

> Login do admin com nosso email e senha padrão

----------
**Oni**

Rua Pastor Hollerbach, 256, Mucuri Coworking. Centro. Teófilo Otoni

(33) 3521-2001

www.onimarketing.com.br

www.onidigital.com