const { mix } = require('laravel-mix');
mix
	// Javascripts do SITE
   .js([
      'resources/assets/bower/jquery/dist/jquery.js',
      'resources/assets/bower/jquery-ui/jquery-ui.min.js',
      'resources/assets/bower/bootstrap/dist/js/bootstrap.min.js',

      'resources/assets/js/bibliotecas/masterslider.min.js',
      'resources/assets/js/site.js'
   ], 'public/js/site.js')
   
   // Javascripts do ADMIN
   .js([
      'resources/assets/bower/jquery/dist/jquery.js',
      'resources/assets/bower/jquery-ui/jquery-ui.min.js',
      'resources/assets/bower/bootstrap/dist/js/bootstrap.min.js',
      'resources/assets/bower/moment/moment.js',
      'resources/assets/bower/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
      'resources/assets/js/bibliotecas/trumbowyg.js',
      'resources/assets/js/bibliotecas/trumbowyg.pt.min.js',
      'resources/assets/bower/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js',
      'resources/assets/bower/jquery-validation/dist/jquery.validate.min.js',
      'resources/assets/bower/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js',
      'resources/assets/bower/chartist/dist/chartist.min.js',
      'resources/assets/bower/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js',
      'resources/assets/bower/bootstrap-notify/js/bootstrap-notify.js',
      'resources/assets/bower/sharrre/jquery.sharrre.min.js',
      'resources/assets/bower/jvectormap/jquery-jvectormap.js',
      'resources/assets/bower/nouislider/distribute/nouislider.min.js',
      'resources/assets/bower/bootstrap-select/dist/js/bootstrap-select.min.js',
      'resources/assets/bower/sweetalert2/dist/sweetalert2.min.js',
      'resources/assets/bower/jasny-bootstrap/dist/js/jasny-bootstrap.min.js',
      'resources/assets/bower/fullcalendar/dist/fullcalendar.min.js',
      'resources/assets/bower/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
    //   'resources/assets/bower/jquery.maskedinput/dist/jquery.maskedinput.js',

      // Fim dependencias via Bower
      'resources/assets/js/bibliotecas/bootstrap-switch.min.js',
      'resources/assets/js/bibliotecas/paper-dashboard.js',
      'resources/assets/js/admin.js'
   ], 'public/js/admin.js')

   .sass('resources/assets/sass/site.scss', 'public/css')
   .sass('resources/assets/sass/admin.scss', 'public/css')

   // Site
   .combine([
      'resources/assets/bower/bootstrap/dist/css/bootstrap.min.css',
      // 'resources/assets/masterslider/style/masterslider.css',
      // 'resources/assets/masterslider/skins/metro/style.css',
      // 'resources/assets/masterslider/ms-fullscreen.css',
      'public/css/site.css'
   ], 'public/css/site.min.css')

   // Admin
   .combine([
      'resources/assets/bower/bootstrap/dist/css/bootstrap.min.css',
      'resources/assets/css/font-awesome.min.css',
      'resources/assets/css/themify-icons.css',
      'resources/assets/css/muli-font.css',
      'resources/assets/css/paper-dashboard.css',      
      'resources/assets/css/trumbowyg.css',
      'public/css/admin.css'
   ], 'public/css/admin.min.css')

   .copy('resources/assets/img', 'public/img')

   // Fontes
   .copy('resources/assets/bower/components-font-awesome/fonts', 'public/fonts')
   .copy('resources/assets/fonts', 'public/fonts')

;