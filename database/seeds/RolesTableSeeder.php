<?php
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
            'name'     => 'admin',
        ];
        $role = Role::create($role);
        // Associamos todas as permissões atuais ao papel:
        $role->givePermissionTo(Permission::pluck('name')->all());

        // Papel padrão para visitante:
        $role2 = [
            'name'     => 'master',
        ];
        $role2 = Role::create($role2);
        
        // Papel padrão para visitante:
        $role3 = [
            'name'     => 'visitante',
        ];
        $role3 = Role::create($role3);
    }
}
