<?php 
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permissions;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(config('permissoes.itens') as $nome => $acoes){
            foreach($acoes as $key => $acao){
                \Spatie\Permission\Models\Permission::updateOrCreate(['name' => $acao]);
            }
        }
    }
}