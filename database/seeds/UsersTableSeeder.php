<?php
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'name'     => 'Administrador',
            'email'    => 'admin@onidigital.com',
            'password' => bcrypt('dominaromundo'),
        ];
        $user = User::create($user);

        // Associa a uma role:
        if(Schema::hasTable('roles')){
        	// busca a primeira role e associa:
        	$role = Role::orderBy('id')->first();
        	if($role !== null){
        		$user->assignRole($role->name);
        	}
        }

        $user2 = [
            'name'     => 'Usuario Master',
            'email'    => 'master@onidigital.com',
            'password' => bcrypt('master123456'),
        ];
        $user2 = User::create($user2);

        // Associa a uma role master:
        if(Schema::hasTable('roles')){
    		$user2->assignRole('master');
        }


    }
}
