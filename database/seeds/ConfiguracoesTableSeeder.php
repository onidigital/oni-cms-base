<?php 
use Illuminate\Database\Seeder;
use App\Models\Configuracao;

class ConfiguracoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configuracoes')->truncate();

        $c = [
            'site_nome'       => 'Oni Site',
        ];
        $c = Configuracao::create($c);

    }
}