<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserPasswordNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            /**
             * Informar ao banco de dados que a tabela de senha será nula
             * pois para o login social não teremos que pedir senha ao usuário, 
             * já que é justamente o motivo de estarmos usando este recurso(pra evitar que o indivíduo memorize várias senhas)
             */
            $table->string('password')->nullable()->change();

            /**
             * Add facebook id para saber que o usuário está vinculado a uma conta do fb:
             */
            $table->string('facebook_id')->nullable()->unique()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

            // Volta o campo pra NOT NULL:
            //$table->string('password')->nullable(false)->change();
            $table->dropColumn('facebook_id');
        });
    }
}
