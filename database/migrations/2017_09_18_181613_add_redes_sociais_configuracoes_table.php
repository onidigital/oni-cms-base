<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRedesSociaisConfiguracoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configuracoes', function (Blueprint $table) {
            $table->string('facebook')->after('site_palavras_chave')->nullable();
            $table->string('instagram')->after('facebook')->nullable();
            $table->string('twitter')->after('instagram')->nullable();
            $table->string('youtube')->after('twitter')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configuracoes', function (Blueprint $table) {
            $table->dropColumn(['facebook', 'instagram', 'twitter', 'youtube']);
        });
    }
}
