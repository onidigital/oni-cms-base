<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuracoes', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('site_nome')->default('Nome do site')->nullable();
            $table->string('site_descricao')->nullable();
            $table->string('site_palavras_chave')->nullable();
            $table->boolean('site_publicado')->default(0)->nullable();
            $table->boolean('debug')->default(1)->comment('para liberar ou não o debug do sistema');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configuracoes');
    }
}
