<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu do dashboard
    |--------------------------------------------------------------------------
    |
    | Insira aqui os menus que serão utilizados no projeto.
    |
    | Link para a lista de ícones disponíveis do Material Design:
    | font-awesome ou themify-icons
    |
    | *** OBS: para o menu aparecer, insira também as permissões no arquivo config/permissoes.php
    */

    'itens' => [

    	// Users
    	'Usuários' => [
    		// ao clicar no menu, pra onde vamos?
	        'url'      	 => 'admin/users',
	        /** 
	         * Quais permissões todo este item abrange?
	         * ['listar entidade', 'inserir entidade', 'editar entidade', 'excluir entidade']
	         */
	        'permissoes' => ['listar usuarios'],
	        // icone do menu
 	        'icone'    	 => 'ti-user',
 	        // submenu é apenas um array com mais menus no modelo acima
 	        // Modelo:
 	        /*
	        'submenus'   => [
	        	
	        	'Listar' => [
	        		'url'        => 'admin/users',
	        		'icone'      => '',
	        		'permissoes' => ['listar usuarios'] 
	        	],

	        	'Cadastrar' => [
	        		'url'        => 'admin/users/create',
	        		'icone'      => '',
	        		'permissoes' => ['inserir usuarios'] 
	        	]
	       	],*/
	    ],

	    // 
		'Configurações gerais' => [
	        'url'        => 'admin/configuracoes',
	        'icone'      => 'fa fa-gear',
	        'permissoes' => ['listar configuracoes'],
	        'submenus'   => []
	    ],

	    // 
		'Sentenças' => [
	        'url'        => 'admin/sentencas',
	        'icone'      => 'ti-list',
	        'permissoes' => ['listar sentencas'],
	        'submenus'   => []
	    ],

	    // Roles
		'Papéis e Permissões' => [
	        'url'        => 'admin/roles',
	        'icone'      => 'fa fa-group',
	        'permissoes' => ['listar papeis'],
	        'submenus'   => []
	    ],

	    // Logs
	    /*
		'Registro de atividades' => [
	        'url'      => 'admin/logs',
	        'icone'    => 'find_in_page',
	        'permissoes' => ['listar logs'],
	        'submenus' => []
	    ],*/
    ],

];
