<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Todas as permissões disponíveis no projeto
    |--------------------------------------------------------------------------
    |
    | Este arquivo é pra guardar as possíveis permissões que serão utilizadas
    | pelos papéis. Assim não precisamos cadastrar uma a uma durante o desenvolvimento, 
    | já que serão fixas por projeto.
    | O formato do array abaixo é para auxiliar na hora da listagem.
    | O que é utilizado mesmo é a informação "inserir entidade", "excluir entidade" ...
    | Convenção: tudo minúsculo, entidade sem acentos e no plural, exemplo: "inserir usuarios"
    | Referência do pacote utilizado: https://github.com/spatie/laravel-permission
    |
    */

    'itens' => [
    	// Users
    	'Usuários' => [
	        'Inserir' => 'inserir usuarios',
	        'Editar'  => 'editar usuarios',
	        'Excluir' => 'excluir usuarios',
	        'Listar'  => 'listar usuarios',
	    ],
        // 
        'Configurações gerais' => [
            'Inserir' => 'inserir configuracoes',
            'Editar'  => 'editar configuracoes',
            'Excluir' => 'excluir configuracoes',
            'Listar'  => 'listar configuracoes',
        ],
        // 
        'Sentenças' => [
            'Inserir' => 'inserir sentencas',
            'Editar'  => 'editar sentencas',
            'Excluir' => 'excluir sentencas',
            'Listar'  => 'listar sentencas',
        ],
	    // Roles
		'Papéis' => [
	        'Inserir' => 'inserir papeis',
	        'Editar'  => 'editar papeis',
	        'Excluir' => 'excluir papeis',
	        'Listar'  => 'listar papeis',
	    ],
        // Logs
        'Registro de atividades' => [
            'Inserir' => 'inserir logs',
            'Editar'  => 'editar logs',
            'Excluir' => 'excluir logs',
            'Listar'  => 'listar logs',
        ],
    ],

];
