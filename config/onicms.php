<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Configurações do CMS
    |--------------------------------------------------------------------------
    |
    | Aqui podemos inserir os labels de botões entre outras configurações
    |
    */

    'titulo_painel' => 'Painel de Controle',

    /**
     * Labels de botões e menus fixos:
     */
    'labels' => [
        'botoes' => [
            'botao_cadastrar' => 'Novo',
            'botao_salvar'    => 'Inserir',
            'botao_atualizar' => 'Atualizar',
        ],
    ],

    /**
     * Mensagens padrão que vão após eventos rotineiros:
     */
    'mensagens' => [
        'sucesso' => [
            'add_registro'    => 'Registro adicionado com sucesso!',
            'update_registro' => 'Registro atualizado com sucesso!',
            'destroy_registro' => 'Registro removido',
        ],
        'erro' => [
            'add_registro'            => 'Ocorreu um erro ao adicionar o registro',
            'update_registro'         => 'Ocorreu um erro ao atualizar o registro',
            'registro_nao_encontrado' => 'Registro não encontrado',
            'nao_autorizado'          => 'Você não tem permissão para realizar esta ação',
            'destroy_registro'        => 'Erro ao remover o registro',
        ],
    ],

    'alertas' => [
        'excluir_registro' => 'Tem certeza que deseja excluir este registro?',
    ],

    // Textos de ajuda do sistema:
    'help_text' => [
        'configuracoes' => [
            'site_publicado' => 'Caso não esteja publicado, só o administrador logado no sistema e no mesmo navegador poderá visualizar o site. Se publicado estará aberto ao público geral.',
            'debug'          => 'Modo para desenvolvedor: se ativado irá habilidar a visualização em tempo real das estatísticas e erros',
        ],
    ],

];
