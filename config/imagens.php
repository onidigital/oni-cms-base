<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Configurações das dimensões de imagens
    |--------------------------------------------------------------------------
    |
    | 'tabela' => [
    |     'nome_dimensao1' => [with, height, formato],
    |     'nome_dimensao2' => [with, height, formato',
    | ]
    | 
    | * para transparência o formato deve ser PNG
    |
    */

    // Tabela usuários:
    'users' => [
        'user_foto' => [
            'thumb'  => [80, 80],
            'media'  => [160, 160],
        ]
    ],

    // Tabela configuracoes:
    'configuracoes' => [
        'site_logo' => [
            'thumb'   => [80, 80, 'jpg'],
            'grande'  => [250, 250, ''],
        ]
    ],

];
